package org.tiny.web;

import fi.iki.elonen.NanoHTTPD;

import java.io.File;
import java.nio.file.FileSystemException;
import java.util.List;
import java.util.Map;

/**
 * The Tiny Web server is pretty simple concept which opens ultimately simple way
 * for creation of dedicated web-applications, local, data-processing tools and interactive demos/prototypes.
 * It provides "one-click" interface for running local web-server which allows reading and writing content
 * to the resource directory of the application.
 *
 * TODO:
 * 1. Add support for zuss
 * 2. Refine code
 * 3. SQL Lite support
 *
 */
public class TinyWebServer extends NanoHTTPD {
	private WebModule[] modules;

	public TinyWebServer(int port, WebModule ... modules) {
		super(port);
		this.modules = modules;
	}

	@Override
	public Response serve(IHTTPSession session) {
		String uri = session.getUri();
		Method method = session.getMethod();

		System.out.println("Request: " + uri);

		for (WebModule module: modules) {
			if (module.canServe(uri, method)) {
				return module.serve(session);
			}
		}

		return super.serve(session);
	}
}
