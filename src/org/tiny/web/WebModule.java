package org.tiny.web;

import fi.iki.elonen.NanoHTTPD;

/**
 * Web server extension module interface
 */
public interface WebModule {

	/**
	 * Checks can this module server following URI
	 * @param uri the requested URI
	 * @param method request method
	 * @return <c>true</c> if module can server this uri
	 */
	public boolean canServe(String uri, NanoHTTPD.Method method);

	/**
	 * Serves URI
	 * @param session the request session
	 * @return response object
	 */
	public NanoHTTPD.Response serve(NanoHTTPD.IHTTPSession session);
}
