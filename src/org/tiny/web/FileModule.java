package org.tiny.web;

import fi.iki.elonen.NanoHTTPD;
import fi.iki.elonen.NanoHTTPD.*;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FileModule implements WebModule {
	private final static String MIME_PLAINTEXT = NanoHTTPD.MIME_PLAINTEXT;
	private final static String MIME_HTML = NanoHTTPD.MIME_HTML;

	private final File directory;
	private final boolean canWrite;

	/**
	 * Hashtable mapping (String)FILENAME_EXTENSION -> (String)MIME_TYPE
	 */
	private static final Map<String, String> MIME_TYPES = new HashMap<String, String>() {{
		put("css", "text/css");
		put("js", "application/javascript");
		put("json", "application/json");
		put("htm", "text/html");
		put("html", "text/html");
		put("xml", "text/xml");
		put("java", "text/x-java-source, text/java");
		put("md", "text/plain");
		put("txt", "text/plain");
		put("asc", "text/plain");
		put("gif", "image/gif");
		put("jpg", "image/jpeg");
		put("jpeg", "image/jpeg");
		put("png", "image/png");
		put("mp3", "audio/mpeg");
		put("m3u", "audio/mpeg-url");
		put("mp4", "video/mp4");
		put("ogv", "video/ogg");
		put("flv", "video/x-flv");
		put("mov", "video/quicktime");
		put("swf", "application/x-shockwave-flash");
		put("js", "application/javascript");
		put("pdf", "application/pdf");
		put("doc", "application/msword");
		put("ogg", "application/x-ogg");
		put("zip", "application/octet-stream");
		put("exe", "application/octet-stream");
		put("class", "application/octet-stream");
	}};

	public FileModule(File directory, boolean canWrite) {
		this.directory = ensureDir(directory.getAbsoluteFile(), canWrite);
		this.canWrite = canWrite;
	}

	public File getDirectory() {
		return directory;
	}

	private static File ensureDir(File dir, boolean canWrite) {
		if (dir.exists()) {
			if (dir.isDirectory()) {
				return dir;
			}
		} else {
			if (canWrite) {
				if (dir.mkdirs()) return dir; else throw new WebFSException("Writable directory '" + dir.getAbsolutePath() + "' can not be created");
			}
		}
		throw new WebFSException("Path '" + dir.getAbsolutePath() + "' is expected to be a directory");
	}

	@Override
	public boolean canServe(String uri, Method method) {
		return true;
	}

	@Override
	public Response serve(IHTTPSession session) {
		Method method = session.getMethod();

		File file = uriToPath(session.getUri()).toFile();

		switch (method) {
			case GET:
				return getFile(file, session);
			case POST:
				if (canWrite) {
					return storeFile(file, session);
				}
			default:
				return new Response(Response.Status.METHOD_NOT_ALLOWED, MIME_PLAINTEXT,  "Invalid method " + method + " for URI '" + session.getUri() + "'");
		}
	}

	protected Path uriToPath(String uri) {
		return Paths.get(directory.getAbsolutePath(), uri);
	}

	protected String eTagOf(File file) {
		return Integer.toHexString((file.getAbsolutePath() + file.lastModified() + "" + file.length()).hashCode());
	}

	private static Pattern EXTENSION_PATTERN = Pattern.compile(".*\\.([a-z0-9]+)");
	protected String mimeTypeOf(File file) {
		String mimeType = null;
		String name = file.getName().toLowerCase();

		Matcher matcher = EXTENSION_PATTERN.matcher(name);
		if (matcher.matches()) {
			mimeType = MIME_TYPES.get(matcher.group(1));
		}

		return null != mimeType ? mimeType : MIME_PLAINTEXT;
	}

	public Response getFile(File file, IHTTPSession session) {
		if (file.exists()) {
			if (file.isDirectory()) {
				//directory ...
				System.out.println("Directory...");
			} else {
				String serverETag = eTagOf(file);
				String mimeType = mimeTypeOf(file);
				String clientETag = session.getHeaders().get("if-none-match");

				if (serverETag.equals(clientETag)) {
					//just tell to browser that file was not modified
					return createResponse(Response.Status.NOT_MODIFIED, mimeType, "");
				} else {
					//sending content now
					try {
						return new Response(Response.Status.OK, mimeType, new FileInputStream(file));
					} catch (FileNotFoundException e) {
						e.printStackTrace();
					}
				}
			}
		}
		return createResponse(Response.Status.NOT_FOUND, MIME_PLAINTEXT,  "Resource '" + session.getUri() + "' is not found");
	}

	public Response storeFile(File file, IHTTPSession session) {
		try {
			session.parseBody(new HashMap<String, String>());
			String content = session.getParms().get("content");

			if (null != content) {
				FileOutputStream out = new FileOutputStream(file);
				PrintWriter writer = new PrintWriter(out);

				writer.write(content);
				writer.close();
			}

			return createResponse(Response.Status.OK, MIME_PLAINTEXT,  "Updated '" + session.getUri() + "'");

		} catch (Exception e) {
			e.printStackTrace();
			return createResponse(Response.Status.INTERNAL_ERROR, MIME_PLAINTEXT,  "Resource '" + session.getUri() + "' can not be updated");
		}
	}

	private Response createResponse(Response.Status status, String mimeType, String message) {
		Response res = new Response(status, mimeType, message);
		return res;
	}

	private Response createResponse(Response.Status status, String mimeType, InputStream stream) {
		Response res = new Response(status, mimeType, stream);
		return res;
	}
}
