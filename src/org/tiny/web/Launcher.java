package org.tiny.web;

import java.awt.*;
import java.io.IOException;
import java.net.URI;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Entry point for this application
 * Params which are supported:
 * -app={valid path for application home}, default - working dir
 * -data={valid path for writable data}, default = {root}/data
 * -{listener port}, integer value for listening, default = 8000
 * -autorun - automatically launches browser on start
 */
public class Launcher {
	private static final Pattern ARG_APP_PATTERN = Pattern.compile("-app=(.*)");
	private static final Pattern ARG_DATA_PATTERN = Pattern.compile("-data=(.*)");
	private static final Pattern ARG_PORT_PATTERN = Pattern.compile("-(\\d{1,5})");
	private static final Pattern ARG_AUTO_PATTERN = Pattern.compile("-autorun");

	public static final String DEFAULT_PORT = "99";
	public static final String DEFAULT_DATA_DIR = "data";
	public static final String DEFAULT_APP_DIR = "";
	public static final String AUTO_START_URI_BASE = "http://localhost:";

	public static void main(String[] args) throws IOException{

		String appDir = parseArg(args, ARG_APP_PATTERN, DEFAULT_APP_DIR);
		String dataDir = parseArg(args, ARG_DATA_PATTERN, appDir + "/" + DEFAULT_DATA_DIR);
		int port = Integer.valueOf(parseArg(args, ARG_PORT_PATTERN, DEFAULT_PORT));
		boolean autoRun = null != parseArg(args, ARG_AUTO_PATTERN, null);

		TinyWebServer webServer = new TinyWebServer(port, new UserDataModule(dataDir), new ApplicationModule(appDir));
		webServer.start();

		if (autoRun) {
			//this is commend over there...
			openBrowser(port);
		}

		for(;webServer.isAlive();) {
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	private static String parseArg(String[] args, Pattern pattern, String defaultValue) {
		for (String arg: args) {
			Matcher matcher = pattern.matcher(arg);
			if (matcher.matches()) {
				return matcher.group(1);
			}
		}
		return defaultValue;
	}

	private static void openBrowser(int port) {
		try {
			Desktop.getDesktop().browse(new URI(AUTO_START_URI_BASE + port));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
