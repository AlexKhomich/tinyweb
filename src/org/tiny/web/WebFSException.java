package org.tiny.web;

/**
 * Explains exceptional situation when access to application file system
 */
public class WebFSException extends  RuntimeException {

	public WebFSException(String message) {
		super(message);
	}

	public WebFSException(String message, Throwable cause) {
		super(message, cause);
	}
}
