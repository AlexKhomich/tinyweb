package org.tiny.web;

import fi.iki.elonen.NanoHTTPD;

import java.io.File;
import java.nio.file.Path;

public class UserDataModule extends FileModule {
	private final static String DATA_URI_PREFIX = "/data";

	public UserDataModule(String dir) {
		super(new File(dir), true);
	}

	@Override
	public boolean canServe(String uri, NanoHTTPD.Method method) {
		return uri.startsWith(DATA_URI_PREFIX);
	}

	@Override
	protected Path uriToPath(String uri) {
		return super.uriToPath(uri.substring(DATA_URI_PREFIX.length()));
	}
}
