package org.tiny.web;

import java.io.File;

public class ApplicationModule extends FileModule {
	public ApplicationModule(String dir) {
		super(new File(dir), false);
	}
}
