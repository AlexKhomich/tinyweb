var EditorIO = new function() {
    this.bind = function(editor) {
        //load content
        $.get('./data/model.json', function(model) {
            $.get('./data/design.json', function(design) {
                Editor.loadModel(model, design);

                //set on update design handler
                Editor.onUpdate = function(design) {
                    $.post('./data/design.json', { content: JSON.stringify(design) });
                }
            });
        });
    }
};