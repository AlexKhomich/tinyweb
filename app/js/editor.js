function Editor(selector) {
    var _me = this;
    var _nodes = {};
    var _links = [];
    var _linkDef = [];

    var _design = {}
    var _snap = Snap(selector);


    function _clear() {
        _.each(_nodes, function(el) { el.remove(); })
    }

    function _rebuild() {
        _snap = _init(_snap);
    }

    function _relinkNodes(linkDef) {
        _.each(_links, function(l) {l.remove();});

        var _linkStory = {
            midX: _.reduce(_nodes, function(sum, node) { return sum + node.location().x; }, 0) / _.values(_nodes).length
        };

        _links = _.map(linkDef, function(link) {
            return new NodeLink(
                _nodes[link.from.node], link.from.key,
                _nodes[link.to.node], link.to.key,
                _snap,
                _linkStory
            )
        });
    }

    function _endUpdate() {
        var nodes = {};
        _.each(_nodes, function(node) {
            nodes[node.id] = node.location();
        })

        var design = {nodes: nodes};

        _me.onUpdate(design);
    }

    this.loadModel = function(model, design) {
        _clear();

        _nodes = {};
        for (var nodeName in model.nodes) {
            var nodeInfo = model.nodes[nodeName];
            var designInfo = design.nodes[nodeName];
            _nodes[nodeName] = new EditorNode(nodeName, nodeInfo, designInfo, _snap);

            //test for determining key location
            _.each(_nodes, function(node) {
                var kl = node.getKeyLocation("ID");
                if (kl) {
                   // _snap.circle(kl.left.x, kl.left.y, 2);
                   // _snap.circle(kl.right.x, kl.right.y, 2);
                }
            });
        }

        _relinkNodes(_linkDef = model.links);
    }

    this.onUpdate = function() {}

    function EditorNode(nodeId, node, design, snap) {
        var _me = this;
        var _body = snap.group();
        var _keyCount = node.keys ? node.keys.length : 0;
        var _keys = [];
        design = design || {x: 10, y: 10};

        var _bounding = _body.rect(0, 0, _Const.NodeWidth, 2 + (1 + _keyCount) * _Const.ItemHeight).attr({fill: 'white', stroke: 'black'});
        var _text = _body.text(_Const.NodeWidth / 2, _Const.ItemHeight / 2, nodeId).attr({'font-family': 'Arial', 'font-size': '10px', 'text-anchor': 'middle', 'dominant-baseline':'middle'});
        _.each(node.keys || [], function(key, idx) {
            var dy = (1 + idx) * _Const.ItemHeight;
            var bgColor = _Const.KeyColors[idx % _Const.KeyColors.length];
            _keys.push({
                id: key,
                index: idx,
                bar: _body.rect(2, dy + 1, _Const.NodeWidth - 4, _Const.ItemHeight - 1).attr({'fill': bgColor}),
                text: _body.text(_Const.NodeWidth / 2, dy + _Const.ItemHeight / 2, key).attr({'font-family': 'Arial', 'font-size': '10px', 'text-anchor': 'middle', 'dominant-baseline':'middle'})
            });
        });

        _body.attr({transform: 't' + [design.x, design.y]});

        //var origTransform;
        var _originalXy;
        _body.drag(function (dx, dy) {
            this.attr({
                transform: origTransform + (origTransform ? "T" : "t") + [dx, dy]
            });
            _relinkNodes(_linkDef);
        }, function () {
            origTransform = this.transform().local;
        }, function() {
            _endUpdate();
        });


        //var box = _body.getBBox();
        //console.log(box);

        this.id = nodeId;

        this.location = function() {
            var xy = _body.transform().local.substring(1).split(',');
            return { x: parseInt(xy[0]), y: parseInt(xy[1]) };
        }

        this.getKeyLocation = function(keyId) {
            var xy = this.location();
            var key = _.find(_keys, function(k) { return k.id == keyId; });
            if (key) {
                var bbox = key.bar.getBBox();
                var topOffset =xy.y + bbox.y + _Const.ItemHeight / 2;
                return {
                    left: { nodeId: nodeId, key: keyId, x: xy.x, y: topOffset },
                    right: { nodeId: nodeId, key: keyId, x: xy.x + _Const.NodeWidth, y: topOffset }
                };
            } else {
                return null;
            }
        }

        this.remove = function() {
            _body.remove();
        }
    }

    function NodeLink(fromNode, fromKey, toNode, toKey, snap, story) {
        var _from = fromNode.getKeyLocation(fromKey);
        var _to = toNode.getKeyLocation(toKey);
        var _body = snap.group();

        //var _line = _body.line(_to.left.x, _to.left.y, _from.left.x + 2, _from.left.y).attr({'stroke-width': '1', 'stroke': 'black'});

        function _overlaps(a, b) {
            return Math.abs(a.left.x - b.left.x) < (_Const.NodeWidth + _Const.SafePad * 2);
        }

        function _path(from, to, dir) {
            var fromKey = from.nodeId + "." + from.key;
            var toKey = to.nodeId + "." + to.key;

            var fromStory = story[fromKey] || { refs: 0 };
            var toStory = story[toKey] || { refs: 0 };
            console.log("Link Story:", fromKey, fromStory, toKey, toStory);

            return {
                dist: Math.abs(from.x - to.x),
                sx: from.x,
                sy: from.y + _Const.RefOffsets[fromStory.refs],
                ex: to.x,
                ey: to.y + _Const.RefOffsets[toStory.refs],
                mx: (to.x + to.y) / 2,
                dir: dir,
                remember: function() {
                    fromStory.refs += 1;
                    toStory.refs += 1;
                    story[fromKey] = fromStory;
                    story[toKey] = toStory;
                }
            };
        }

        function _selectBestPath() {
            if (_overlaps(_from, _to)) {
                var midX = (_from.left.x + _from.right.x + _to.left.x + _to.right.x) / 4;
                return (midX < story.midX) ? _path(_from.left, _to.left, -1) : _path(_from.right, _to.right, 1);
            } else {
                var a = _path(_from.left, _to.right, 0);
                var b = _path(_from.right, _to.left, 0);
                return a.dist < b.dist ? a : b;
            }
        }

        function _buildPathStr(p) {
            //prepare start
            var pth = "M" + [p.sx, p.sy];
            var padFirst = p.dir == 1 && p.sx >= p.ex || p.dir == -1 && p.sx <= p.ex;
            var px = p.dir * _Const.SafePad * (padFirst ? 1 : -1);    //safe padding
            var lx = p.ex - p.sx - px;
            var hx = lx / 2;

            var dx1 = p.dir ? (padFirst ? px : lx) : hx;
            var dx2 = p.dir ? (padFirst ? lx : px) : hx;
            var dy = p.ey - p.sy;

            //draw H1 segment
            pth += 'l' + [dx1, 0];
            //draw vertical line
            pth += 'l' + [0, dy];
            //draw H2 segment
            pth += 'l' + [dx2, 0];

            return pth;
        }

        //draw path
        var _bp = _selectBestPath();
        _body.path(_buildPathStr(_bp)).attr({'stroke-width': '1', 'stroke': 'black', fill: 'none'});
        _body.circle(_bp.sx, _bp.sy, 2);
        _body.circle(_bp.ex, _bp.ey, 2);
        _bp.remember();

        this.remove = function() {
            _body.remove();
        }
    }
}

var _Const = {
    KeyColors: ["#DFD", "#DDF", "#FDD", "#FFD"],
    NodeWidth: 120,
    ItemHeight: 20,
    SafePad: 20,
    RefOffsets: [0, -6, 6, -3, 3, -9, 9, 5, -5, 8, -8]
}


